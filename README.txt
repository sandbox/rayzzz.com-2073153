CONTENTS OF THIS FILE
---------------------
* Introduction
* Requirements
* Installation
* Configuration


INTRODUCTION
------------
The Audio/Video Chat module provides users
with ability of chatting and broadcasting video and audio
without necessity to install expensive media server.
* For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/rayzzz.com/2073153
* To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2073153
   
   
REQUIREMENTS
------------
This module requires no additional modules.


INSTALLATION
------------
* Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
* Installation manual is available at http://rayzzz.com/docs/chat/install/drupal


CONFIGURATION
-------------
* Configure user permissions in Administration > People > Permissions:
   - Use Video Chat
     This setting grants/denies access to the module.
* Other configurations are available at Audio/Video Chat with admin logged in.
Learn more at http://rayzzz.com/docs/chat/settings

All manuals are available at http://rayzzz.com/docs/chat
