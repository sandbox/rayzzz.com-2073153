<?php

/**
 * @file
 * Module main class.
 */

class RzchatModule extends RzchatIntegration {
  /**
   * Module system name.
   */
  protected $sModule = "rzchat";
  /**
   * Current users db table.
   */
  protected $usersDbTable;
  /**
   * Profiles db table.
   */
  protected $profilesDbTable;
  /**
   * Rooms db table.
   */
  protected $roomsDbTable;
  /**
   * Rooms users db table.
   */
  protected $roomsUsersDbTable;
  /**
   * Messages db table.
   */
  protected $messagesDbTable;
  /**
   * Memberships settings db table.
   */
  protected $memsetsDbTable;
  /**
   * Memberships settings values db table.
   */
  protected $memsDbTable;
  /**
   * Blocked users db table.
   */
  protected $blockedDbTable;
  /**
   * Chat history db table.
   */
  protected $historyDbTable;

  const USER_STATUS_NEW = "new";
  const USER_STATUS_OLD = "old";
  const USER_STATUS_KICK = "kick";
  const USER_STATUS_IDLE = "idle";
  const USER_STATUS_TYPE = "type";
  const USER_STATUS_ONLINE = "online";
  const USER_STATUS_BUSY = "busy";
  const USER_STATUS_AWAY = "away";
  const ROOM_STATUS_NORMAL = "normal";
  const ROOM_STATUS_DELETE = "delete";
  const CHAT_TYPE_MODER = "moder";
  const CHAT_TYPE_FULL = "full";
  const CHAT_TYPE_ADMIN = "admin";

  /**
   * Class constructor.
   */
  public function __construct($s_path, $s_url) {
    parent::__construct($s_path, $s_url);
    $this->usersDbTable = $this->sModule . "_current_users";
    $this->profilesDbTable = $this->sModule . "_profiles";
    $this->roomsDbTable = $this->sModule . "_rooms";
    $this->roomsUsersDbTable = $this->sModule . "_rooms_users";
    $this->messagesDbTable = $this->sModule . "_messages";
    $this->memsetsDbTable = $this->sModule . "_memberships_settings";
    $this->memsDbTable = $this->sModule . "_memberships";
    $this->blockedDbTable = $this->sModule . "_blocked_users";
    $this->historyDbTable = $this->sModule . "_history";
    $this->aXmlTemplates["user"] = array(
      2 => '<user id="#1#" status="#2#" />',
      3 => '<user id="#1#" status="#2#" type="#3#" />',
      4 => '<user id="#1#" status="#2#" type="#3#" online="#4#" />',
      8 => '<user id="#1#" sex="#3#" age="#4#" photo="#5#" profile="#6#" banned="#7#" type="#8#"><nick><![CDATA[#2#]]></nick></user>',
      10 => '<user id="#1#" status="#2#" sex="#4#" age="#5#" photo="#7#" profile="#8#" type="#9#" online="#10#"><nick><![CDATA[#3#]]></nick><desc><![CDATA[#6#]]></desc></user>',
      11 => '<user id="#1#" status="#2#" sex="#4#" age="#5#" photo="#7#" profile="#8#" type="#9#" online="#10#" time="#11#"><nick><![CDATA[#3#]]></nick><desc><![CDATA[#6#]]></desc></user>',
    );
    $this->aXmlTemplates["message"] = array(
      15 => '<message id="#1#" room="#3#" author="#4#" user="#5#" whisper="#6#" color="#7#" bold="#8#" underline="#9#" italic="#10#" size="#11#" font="#12#" smileset="#13#" date="#14#" count="#15#"><text><![CDATA[#2#]]></text></message>',
    );
    $this->aXmlTemplates["file"][4] = '<file user="#1#" file="#2#" count="#4#"><name><![CDATA[#3#]]></name></file>';
    $this->aXmlTemplates["room"] = array(
      2 => '<room id="#1#" status="#2#" />',
      3 => '<room id="#1#" in="#2#" out="#3#" />',
      6 => '<room id="#1#" status="#2#" owner="#3#" password="#4#"><title><![CDATA[#5#]]></title><desc><![CDATA[#6#]]></desc></room>',
      7 => '<room id="#1#" in="#6#" inTime="#7#" owner="#2#" password="#3#"><title><![CDATA[#4#]]></title><desc><![CDATA[#5#]]></desc></room>',
      8 => '<room id="#1#" status="#2#" in="#7#" out="#8#" owner="#3#" password="#4#"><title><![CDATA[#5#]]></title><desc><![CDATA[#6#]]></desc></room>',
    );
    $this->aXmlTemplates["smileset"] = array(
      2 => '<properties current="#1#" url="#2#" />',
      3 => '<smileset folder="#1#" config="#2#"><![CDATA[#3#]]></smileset>',
    );
    $this->aXmlTemplates["history"] = array(
      "user" => array(2 => '<user id="#1#"><![CDATA[#2#]]></user>'),
      "room" => array(3 => '<room id="#1#" title="#2#" count="#3#">'),
      "msg" => array(3 => '<msg id="#1#" sender="#2#"><![CDATA[#3#]]></msg>', 4 => '<msg id="#1#" sender="#2#" recipient="#3#"><![CDATA[#4#]]></msg>'),
      "private" => array(3 => '<private sender="#1#" recipient="#2#" count="#3#">'),
    );
  }

  /**
   * Config generator.
   */
  public function actionConfig() {
    $s_contents = parent::actionConfig();
    $i_file_size = (int) $this->getSettingValue("fileSize");
    $i_max_file_size = min((ini_get('upload_max_filesize') + 0), (ini_get('post_max_size') + 0), $i_file_size);
    $s_contents = str_replace("#fileMaxSize#", $i_max_file_size, $s_contents);
    $s_contents = str_replace("#soundsUrl#", $this->sUrl . "data/sounds/", $s_contents);
    $s_contents = str_replace("#smilesetsUrl#", $this->sUrl . "data/smilesets/", $s_contents);
    $s_contents = str_replace("#loginUrl#", $this->sLoginUrl, $s_contents);
    return $s_contents;
  }

  /**
   * Gets plugins.
   */
  public function actionGetPlugins() {
    $s_contents = "";
    $s_folder = $this->getRequestVar("app") == "admin" ? "pluginsAdmin/" : "plugins/";
    $s_plugins_path = $this->sPath . $s_folder;
    if (is_dir($s_plugins_path)) {
      if ($r_dir_handle = opendir($s_plugins_path)) {
        while (FALSE !== ($s_plugin = readdir($r_dir_handle))) {
          if (strpos($s_plugin, ".swf") === strlen($s_plugin) - 4) {
            $s_contents .= $this->parseXml(array(1 => '<plugin><![CDATA[#1#]]></plugin>'), $this->sUrl . $s_folder . $s_plugin);
          }
        }
      }
      closedir($r_dir_handle);
    }
    return $this->makeGroup($s_contents, "plugins");
  }

  /**
   * Sets font setting.
   */
  public function actionRayzFontSet() {
    $s_key = $this->getRequestVar("key");
    $s_value = $this->getRequestVar("value");
    if (!empty($s_key) && !empty($s_value)) {
      setcookie("RayzFont" . $s_key, $s_value, time() + 31536000);
    }
  }

  /**
   * Gets font setting.
   */
  public function actionRayzFontGet() {
    $a_settings = array(
      8 => '<settings bold="#1#" italic="#2#" underline="#3#" color="#4#" font="#5#" size="#6#" volume="#7#" muted="#8#" />',
    );
    return $this->parseXml($a_settings, $this->getFontSetting("bold"), $this->getFontSetting("italic"), $this->getFontSetting("underline"), $this->getFontSetting("color"), $this->getFontSetting("font"), $this->getFontSetting("size"), $this->getFontSetting("volume"), $this->getFontSetting("muted"));
  }

  /**
   * Gets font setting helper.
   */
  protected function getFontSetting($s_name) {
    return isset($_COOKIE["RayzFont" . $s_name]) ? $_COOKIE["RayzFont" . $s_name] : "";
  }

  /**
   * Gets blocking users.
   */
  public function actionRzGetBlockingUsers() {
    return $this->getBlockUsers(TRUE);
  }

  /**
   * Gets blocked users.
   */
  public function actionRzGetBlockedUsers() {
    return $this->getBlockUsers(FALSE);
  }

  /**
   * Gets blocking(ed) users.
   */
  protected function getBlockUsers($b_blocking) {
    $s_select_field = $b_blocking ? "User" : "Blocked";
    $s_where_field = $b_blocking ? "Blocked" : "User";
    $s_type = db_select($this->profilesDbTable, 't')->fields('t')->condition('ID', $this->sId)->range(0, 1)->execute()->fetchField();
    if (empty($s_type)) {
      $s_type = self::CHAT_TYPE_FULL;
    }
    $a_all_types = array(
      self::CHAT_TYPE_FULL,
      self::CHAT_TYPE_MODER,
      self::CHAT_TYPE_ADMIN,
    );
    $i_type_index = array_search($s_type, $a_all_types);
    if ($b_blocking) {
      array_splice($a_all_types, 0, $i_type_index);
    }
    else {
      array_splice($a_all_types, $i_type_index + 1, count($a_all_types) - $i_type_index - 1);
    }
    $r_db = db_select($this->blockedDbTable, 'blocked')->fields('blocked', array($s_select_field));
    $r_db->leftJoin($this->profilesDbTable, 'profiles', 'blocked.' . $s_select_field . ' = profiles.ID');
    $r_db = $r_db->condition('blocked.' . $s_where_field, $this->sId);
    if (count($a_all_types)) {
      $r_db = $r_db->condition('profiles.Type', $a_all_types, 'IN');
    }
    $r_result = $r_db->execute();
    $a_users = array();
    while (($a_blocked = $r_result->fetchAssoc()) && $a_blocked) {
      $a_users[] = $a_blocked["Member"];
    }
    return $this->parseXml($this->aXmlTemplates['result'], implode(",", $a_users));
  }

  /**
   * Sets blocked user.
   */
  public function actionRzSetBlocked() {
    $s_user = $this->getRequestVar("user");
    $b_blocked = $this->getRequestVar("blocked", "boolean");
    if ($b_blocked) {
      $s_blocked_id = db_select($this->blockedDbTable, 't')->fields('t', array('ID'))->condition(db_and()->condition('User', $this->sId)->condition('Blocked', $s_user))->range(0, 1)->execute()->fetchField();
      if (empty($s_blocked_id)) {
        db_insert($this->blockedDbTable)->fields(array('User' => $this->sId, 'Blocked' => $s_user))->execute();
      }
    }
    else {
      db_delete($this->blockedDbTable)->condition(db_and()->condition('User', $this->sId)->condition('Blocked', $s_user))->execute();
    }
  }

  /**
   * Gets memberships.
   */
  public function actionRayzGetMemberships() {
    $a_memberships = $this->getMemberships();
    $s_memberships = "";
    foreach ($a_memberships as $s_id => $s_name) {
      $s_memberships .= $this->getMembershipValues($s_id, $s_name);
    }
    $s_contents = $this->getMembershipSettings(TRUE);
    $s_contents .= $this->makeGroup($s_memberships, "memberships");
    return $s_contents;
  }

  /**
   * Sets membership setting.
   */
  public function actionRayzSetMembershipSetting() {
    $s_key = $this->getRequestVar("key");
    $s_value = $this->getRequestVar("value");
    $r_db = db_select($this->memsetsDbTable, 'keys');
    $r_db->leftJoin($this->memsDbTable, 'vals', 'keys.ID = vals.Setting');
    $a_keys = $r_db->fields('keys', array('ID'))->fields('vals', array('ID'))->condition('keys.Name', $s_key)->range(0, 1)->execute()->fetchAssoc();
    if (empty($a_keys['ID'])) {
      return $this->parseXml($this->aXmlTemplates['result'], "Error saving setting.", self::FAILED_VAL);
    }
    elseif (empty($a_keys['ID'])) {
      db_insert($this->memsDbTable)->fields(array(
        'Setting' => $a_keys['ID'],
        'Value' => $s_value,
        'Membership' => $this->sId,
      ))->execute();
    }
    else {
      db_update($this->memsDbTable)->fields(array('Value' => $s_value))->condition('ID', $a_keys['ID'])->execute();
    }
  }

  /**
   * Gets membership settings.
   */
  public function actionRayzGetMembership() {
    $s_membership = $this->getMembershipId($this->sId);
    $s_contents = $this->getMembershipSettings(�����);
    $s_contents .= $this->getMembershipValues($s_membership);
    return $s_contents;
  }

  /**
   * Guest login.
   */
  public function actionRzGuestLogin() {
    $s_nick = $this->getRequestVar("nick", "db");
    $s_user_id = $this->searchUser($s_nick, "NickName");
    if (!empty($s_user_id)) {
      return $this->parseXml($this->aXmlTemplates['result'], "RayzGuestError", self::FAILED_VAL);
    }
    db_delete($this->profilesDbTable)->condition('ID', $this->sId)->execute();
    db_insert($this->profilesDbTable)->fields(array(
      'ID' => $this->sId,
      'Type' => self::CHAT_TYPE_FULL,
      'Smileset' => 'default',
    ))->execute();
    $i_current_time = time();
    $s_sex = $this->getRequestVar("sex", "db");
    if (empty($s_sex)) {
      $s_sex = "M";
    }
    $s_desc = $this->getRequestVar("desc", "db");
    $i_age = $this->getRequestVar("age", "int");
    $s_photo = $this->sUrl . "data/" . ($s_sex == "F" ? "female.jpg" : "male.jpg");
    $i_id = (int) db_select($this->usersDbTable, 't')->fields('t', array('ID'))->condition('ID', $this->sId)->range(0, 1)->execute()->fetchField();
    if ($i_id > 0) {
      db_update($this->usersDbTable)->fields(array(
        'Nick' => $s_nick,
        'Sex' => $s_sex,
        'Age' => $i_age,
        'Description' => $s_desc,
        'Photo' => $s_photo,
        'Profile' => '',
        'Start' => $i_current_time,
        'Time' => $i_current_time,
        'Status' => self::USER_STATUS_NEW,
      ))->condition('ID', $this->sId)->execute();
    }
    else {
      db_insert($this->usersDbTable)->fields(array(
        'Nick' => $s_nick,
        'Sex' => $s_sex,
        'Age' => $i_age,
        'Description' => $s_desc,
        'Photo' => $s_photo,
        'Profile' => '',
        'Start' => $i_current_time,
        'Time' => $i_current_time,
        'Status' => self::USER_STATUS_NEW,
      ))->execute();
    }
    db_delete($this->roomsUsersDbTable)->condition('User', $this->sId)->execute();
    $s_contents = $this->parseXml($this->aXmlTemplates['result'], "", self::SUCCESS_VAL);
    $s_contents .= $this->parseXml(array(2 => '<user photo="#1#" profile="#2#" />'), $s_photo, "");
    return $s_contents;
  }

  /**
   * Authorizes user.
   */
  public function actionUserAuthorize() {
    $b_banned = FALSE;
    $s_password = $this->getRequestVar("password", "db");
    if ($this->loginAdmin($this->sId, $s_password)) {
      $a_user_info = $this->getUserInfo($this->sId);
      $a_user = array(
        'id' => $a_user_info['id'],
        'nick' => $a_user_info['nick'],
        'sex' => $a_user_info['sex'],
        'age' => $a_user_info['age'],
        'desc' => $a_user_info['desc'],
        'photo' => $a_user_info['photo'],
        'profile' => $a_user_info['profile'],
        'type' => self::CHAT_TYPE_ADMIN,
      );
    }
    elseif ($this->loginUser($this->sId, $s_password) && ($b_banned = $this->doBan("check", $this->sId)) != TRUE) {
      $a_user = $this->getUserInfo($this->sId);
      $a_user['id'] = $this->sId;
      $a_user['sex'] = $a_user['sex'] == 'female' ? "F" : "M";
      $a_user['type'] = self::CHAT_TYPE_FULL;
    }
    else {
      return $this->parseXml($this->aXmlTemplates['result'], $b_banned ? "msgBanned" : "msgUserAuthenticationFailure", self::FAILED_VAL);
    }
    $a_user = $this->initUser($a_user);
    $s_contents = $this->parseXml($this->aXmlTemplates['result'], "", self::SUCCESS_VAL);
    $s_contents .= $this->parseXml($this->aXmlTemplates['user'], $a_user['id'], self::USER_STATUS_NEW, $a_user['nick'], $a_user['sex'], $a_user['age'], $a_user['desc'], $a_user['photo'], $a_user['profile'], $a_user['type'], self::USER_STATUS_ONLINE);
    return $s_contents;
  }

  /**
   * Bans user.
   */
  public function actionBanUser() {
    $s_banned = $this->getRequestVar("banned", "strbool");
    $s_user_id = db_select($this->profilesDbTable, 't')->fields('t', array('ID'))->condition('ID', $this->sId)->range(0, 1)->execute()->fetchField();
    if (empty($s_user_id)) {
      db_insert($this->profilesDbTable)->fields(array('ID' => $this->sId, 'Banned' => $s_banned))->execute();
    }
    else {
      db_update($this->profilesDbTable)->fields(array('Banned' => $s_banned))->condition('ID', $this->sId)->execute();
    }
  }

  /**
   * Changes user's type.
   */
  public function actionChangeUserType() {
    $s_type = $this->getRequestVar("type", "db");
    $s_user_id = db_select($this->profilesDbTable, 't')->fields('t', array('ID'))->condition('ID', $this->sId)->range(0, 1)->execute()->fetchField();
    if (empty($s_user_id)) {
      db_insert($this->profilesDbTable)->fields(array('ID' => $this->sId, 'Type' => $s_type))->execute();
    }
    else {
      db_update($this->profilesDbTable)->fields(array('Type' => $s_type))->condition('ID', $this->sId)->execute();
    }
  }

  /**
   * Searches for user.
   */
  public function actionSearchUser() {
    $s_param = $this->getRequestVar("param");
    $s_value = $this->getRequestVar("value", "db");
    $s_user_id = $this->searchUser($s_value, $s_param);
    if (empty($s_user_id)) {
      return $this->parseXml($this->aXmlTemplates['result'], "No User Found.", self::FAILED_VAL);
    }
    $a_user = $this->getUserInfo($s_user_id);
    $a_user['sex'] = $a_user['sex'] == "female" ? "F" : "M";
    $a_profile = db_select($this->profilesDbTable, 't')->fields('t')->condition('ID', $s_user_id)->execute()->fetchAssoc();
    if (!is_array($a_profile) || count($a_profile) == 0) {
      $a_profile = array("Banned" => self::FALSE_VAL, "Type" => self::CHAT_TYPE_FULL);
    }
    $s_contents = $this->parseXml($this->aXmlTemplates['result'], "", self::SUCCESS_VAL);
    $s_contents .= $this->parseXml($this->aXmlTemplates['user'], $s_user_id, $a_user['nick'], $a_user['sex'], $a_user['age'], $a_user['photo'], $a_user['profile'], $a_profile['Banned'], $a_profile['Type']);
    return $s_contents;
  }

  /**
   * Gets sounds.
   */
  public function actionGetSounds() {
    $s_file_name = $this->sPath . "data/sounds.xml";
    if (file_exists($s_file_name)) {
      $r_handle = fopen($s_file_name, "rt");
      $s_contents = fread($r_handle, filesize($s_file_name));
      fclose($r_handle);
    }
    else {
      $s_contents = $this->makeGroup("", "items");
    }
    return $s_contents;
  }

  /**
   * Gets smilesets.
   */
  public function actionGetSmilesets() {
    $s_config_file = "config.xml";
    $s_smilesets_path = $this->sPath . "data/smilesets/";
    $a_smilesets = array();
    if ($r_dir_handle = opendir($s_smilesets_path)) {
      while (FALSE !== ($s_dir = readdir($r_dir_handle))) {
        if ($s_dir != "." && $s_dir != ".." && is_dir($s_smilesets_path . $s_dir) && file_exists($s_smilesets_path . $s_dir . "/" . $s_config_file)) {
          $a_smilesets[] = $s_dir;
        }
      }
    }
    closedir($r_dir_handle);
    if (count($a_smilesets) == 0) {
      return $this->parseXml($this->aXmlTemplates['smileset'], "", "") . $this->makeGroup("", "smilesets");
    }
    $s_def_smileset = "default";
    if (isset($_COOKIE["RayzFontsmileset"])) {
      $s_def_smileset = substr($_COOKIE["RayzFontsmileset"], 0, -1);
    }
    if (!in_array($s_def_smileset, $a_smilesets)) {
      $s_def_smileset = $a_smilesets[0];
    }
    $s_user_smileset = db_select($this->profilesDbTable, 't')->fields('t', array('Smileset'))->condition('ID', $this->sId)->range(0, 1)->execute()->fetchField();
    if (empty($s_user_smileset) || !file_exists($s_smilesets_path . $s_user_smileset)) {
      $s_user_smileset = $s_def_smileset;
    }
    $s_contents = $this->parseXml($this->aXmlTemplates['smileset'], $s_user_smileset . "/", $this->sUrl . "data/smilesets/");
    $s_data = "";
    for ($i = 0; $i < count($a_smilesets); $i++) {
      $s_name = $this->getSettingValue("name", $s_smilesets_path . $a_smilesets[$i] . "/config.xml");
      $s_data .= $this->parseXml($this->aXmlTemplates['smileset'], $a_smilesets[$i] . "/", $s_config_file, empty($s_name) ? $a_smilesets[$i] : $s_name);
    }
    $s_contents .= $this->makeGroup($s_data, "smilesets");
    return $s_contents;
  }

  /**
   * Sets smileset.
   */
  public function actionSetSmileset() {
    $s_smileset = $this->getRequestVar("smileset", "db");
    db_update($this->profilesDbTable)->fields(array('Smileset' => $s_smileset))->condition('ID', $this->sId)->execute();
  }

  /**
   * Gets rooms.
   */
  public function actionGetRooms() {
    $this->doRoom('deleteTemp');
    return $this->makeGroup($this->getRooms("all", $this->sId), "rooms");
  }

  /**
   * Creates room.
   */
  public function actionCreateRoom() {
    $b_temp = $this->getRequestVar("temp", "boolean");
    $s_room = $this->getRequestVar("room", "db");
    $s_password = $this->getRequestVar("password", "db");
    $s_desc = $this->getRequestVar("desc", "db");
    $i_room_id = $this->doRoom('insert', $this->sId, 0, $s_room, $s_password, $s_desc, $b_temp);
    if (empty($i_room_id)) {
      return $this->parseXml($this->aXmlTemplates['result'], "msgErrorCreatingRoom", self::FAILED_VAL);
    }
    else {
      return $this->parseXml($this->aXmlTemplates['result'], $i_room_id, self::SUCCESS_VAL);
    }
  }

  /**
   * Edits room.
   */
  public function actionEditRoom() {
    $i_room_id = $this->getRequestVar("roomId", "int");
    $s_room = $this->getRequestVar("room", "db");
    $s_password = $this->getRequestVar("password", "db");
    $s_desc = $this->getRequestVar("desc", "db");
    $this->doRoom('update', 0, $i_room_id, $s_room, $s_password, $s_desc);
    return $this->parseXml($this->aXmlTemplates['result'], "", self::SUCCESS_VAL);
  }

  /**
   * Deletes room.
   */
  public function actionDeleteRoom() {
    $i_room_id = $this->getRequestVar("roomId", "int");
    $this->doRoom('delete', 0, $i_room_id);
    return $this->parseXml($this->aXmlTemplates['result'], self::TRUE_VAL);
  }

  /**
   * Enter room.
   */
  public function actionEnterRoom() {
    $i_room_id = $this->getRequestVar("roomId", "int");
    $this->doRoom('enter', $this->sId, $i_room_id);
  }

  /**
   * Exit room.
   */
  public function actionExitRoom() {
    $i_room_id = $this->getRequestVar("roomId", "int");
    $this->doRoom('exit', $this->sId, $i_room_id);
  }

  /**
   * Checks room's password.
   */
  public function actionCheckRoomPassword() {
    $i_room_id = $this->getRequestVar("roomId", "int");
    $s_password = $this->getRequestVar("password", "db");
    $s_id = db_select($this->roomsDbTable, 't')->fields('t', array('ID'))->condition(db_and()->condition('ID', $i_room_id)->condition('Password', $s_password))->range(0, 1)->execute()->fetchField();
    if (empty($s_id)) {
      return $this->parseXml($this->aXmlTemplates['result'], "msgWrongRoomPassword", self::FAILED_VAL);
    }
    else {
      return $this->parseXml($this->aXmlTemplates['result'], "", self::SUCCESS_VAL);
    }
  }

  /**
   * Gets online users.
   */
  public function actionGetOnlineUsers() {
    $i_count = (int) db_select($this->usersDbTable, 't')->fields('t', array('ID'))->countQuery()->execute()->fetchField();
    if ($i_count == 0) {
      db_query("TRUNCATE TABLE {rzchat_current_users}");
    }
    $i_count = (int) db_select($this->messagesDbTable, 't')->fields('t', array('ID'))->countQuery()->execute()->fetchField();
    if ($i_count == 0) {
      db_query("TRUNCATE TABLE {rzchat_messages}");
    }
    return $this->refreshUsersInfo($this->sId);
  }

  /**
   * Changes online status.
   */
  public function actionSetOnline() {
    $s_online = $this->getRequestVar("online", "db");
    db_update($this->usersDbTable)->fields(array(
      'Online' => $s_online,
      'Time' => time(),
      'Status' => self::USER_STATUS_ONLINE,
    ))->condition('ID', $this->sId)->execute();
  }

  /**
   * Gets an update.
   */
  public function actionUpdate() {
    $s_files = "";
    $i_last_update = $this->getLastUpdate($this->sId);
    $res = db_select($this->messagesDbTable, 't')->fields('t')->condition(db_and()->condition('Type', 'file')->condition('Recipient', $this->sId))->execute();
    while (($a_file = $res->fetchAssoc()) && $a_file) {
      $s_file_name = $a_file['ID'] . ".file";
      if (!file_exists($this->sFilesPath . $s_file_name)) {
        continue;
      }
      $s_files .= $this->parseXml($this->aXmlTemplates['file'], $a_file['Sender'], $s_file_name, $a_file['Message'], $a_file['Count']);
    }
    db_delete($this->messagesDbTable)->condition(db_and()->condition('Type', 'file')->condition('Recipient', $this->sId))->execute();
    $s_contents = $this->makeGroup($s_files, "files");
    $s_contents .= $this->refreshUsersInfo($this->sId, 'update', $i_last_update);
    $s_contents .= $this->makeGroup($this->getRooms('update', $this->sId, $i_last_update), "rooms");
    $s_contents .= $this->makeGroup($this->getRooms('updateUsers', $this->sId, $i_last_update), "roomsUsers");
    $s_msgs = "";
	$s_rooms = db_query("SELECT GROUP_CONCAT(DISTINCT `Room` SEPARATOR ',') FROM {rzchat_rooms_users} WHERE User=':id' AND Status='" . self::ROOM_STATUS_NORMAL . "'", array(':id' => $this->sId))->fetchField();
    if (empty($s_rooms)) {
      $s_rooms = "''";
    }
    $res = db_select($this->messagesDbTable, 't')->fields('t')->condition(db_and()->condition('Type', 'text')->condition('Sender', $this->sId, '<>')->condition(db_or()->condition(db_and()->condition('Room', explode(',', $s_rooms), 'IN')->condition('Whisper', self::FALSE_VAL))->condition('Recipient', $this->sId))->condition('Time', $i_last_update, '>='))->execute();
    while (($a_msg = $res->fetchAssoc()) && $a_msg) {
      $a_style = unserialize($a_msg['Style']);
      $s_msgs .= $this->parseXml($this->aXmlTemplates['message'], $a_msg['ID'], stripslashes($a_msg['Message']), $a_msg['Room'], $a_msg['Sender'], $a_msg['Recipient'], $a_msg['Whisper'], $a_style['color'], $a_style['bold'], $a_style['underline'], $a_style['italic'], $a_style['size'], $a_style['font'], $a_style['smileset'], $a_msg['Time'], $a_msg['Count']);
    }
    $s_contents .= $this->makeGroup($s_msgs, "messages");
    return $s_contents;
  }

  /**
   * Adds new message.
   */
  public function actionNewMessage() {
    $s_sender = $this->getRequestVar("sender", "db");
    if (empty($s_sender)) {
      return;
    }
    $s_whisper = $this->getRequestVar("whisper", "strbool");
    $i_room_id = $this->getRequestVar("roomId", "int");
    $i_count = $this->getRequestVar("count", "int");
    $i_color = $this->getRequestVar("color", "int");
    $s_bold = $this->getRequestVar("bold", "strbool");
    $s_underline = $this->getRequestVar("underline", "strbool");
    $s_italic = $this->getRequestVar("italic", "strbool");
    $i_size = $this->getRequestVar("size", "int");
    if (empty($i_size)) {
      $i_size = 12;
    }
    $s_font = $this->getRequestVar("font", "db");
    if (empty($s_font)) {
      $s_font = "Arial";
    }
    $s_smileset = $this->getRequestVar("smileset", "db");
    $s_rcp = $this->getRequestVar("recipient", "db");
    $s_message = $this->getRequestVar("message", "db");
    $s_style = serialize(array(
      'color' => $i_color,
      'bold' => $s_bold,
      'underline' => $s_underline,
      'italic' => $s_italic,
      'smileset' => $s_smileset,
      'size' => $i_size,
      'font' => $s_font,
    ));
    $i_time = time();
    db_insert($this->messagesDbTable)->fields(array(
      'Room' => $i_room_id,
      'Count' => $i_count,
      'Sender' => $s_sender,
      'Recipient' => $s_rcp,
      'Message' => $s_message,
      'Whisper' => $s_whisper,
      'Style' => $s_style,
      'Time' => $i_time,
    ))->execute();
    if (empty($i_room_id)) {
      $s_snd_rcp = strcmp($s_sender, $s_rcp) < 0 ? $s_sender . "." . $s_rcp : $s_rcp . "." . $s_sender;
    }
    else {
      $s_snd_rcp = "";
    }
    db_insert($this->historyDbTable)->fields(array(
      'Room' => $i_room_id,
      'SndRcp' => $s_snd_rcp,
      'Sender' => $s_sender,
      'Recipient' => $s_rcp,
      'Message' => $s_message,
      'Time' => $i_time,
    ))->execute();
  }

  /**
   * Gets chat history.
   */
  public function actionGetHistory() {
    $i_day = $this->getRequestVar("day", "int");
    $i_month = $this->getRequestVar("month", "int");
    $i_year = $this->getRequestVar("year", "int");
    $i_start_date = mktime(0, 0, 0, $i_month, $i_day, $i_year);
    $i_end_date = mktime(0, 0, 0, $i_month, ($i_day + 1), $i_year);
    $a_users = array();
    $i_count = (int) db_select($this->historyDbTable, 't')->fields('t')->condition(db_and()->condition('Time', $i_start_date, '>=')->condition('Time', $i_end_date, '<'))->countQuery()->execute()->fetchField();
    if ($i_count == 0) {
      return $this->makeGroup("", "users") . $this->makeGroup("", "rooms") . $this->makeGroup("", "privates");
    }
    // Users.
    $s_users = "";
    $a_users = $this->getHistoryUsers($i_start_date, $i_end_date, FALSE);
    foreach ($a_users as $i_user_id => $a_user) {
      $s_users .= $this->parseXml($this->aXmlTemplates['history']['user'], $i_user_id, $a_user['nick']);
    }
    $s_contents = $this->makeGroup($s_users, "users");

    // Rooms dialogs.
    $r_res_rooms = db_select($this->historyDbTable, 'h');
    $r_res_rooms->join($this->roomsDbTable, 'r', 'h.Room = r.ID');
    $r_res_rooms = $r_res_rooms->fields('h')->fields('r', array('Name'))->condition(db_and()->condition('h.Room', 0, '>')->condition('h.Time', $i_start_date, '>=')->condition('h.Time', $i_end_date, '<'))->orderBy('h.Room', 'ASC')->orderBy('h.Time', 'ASC')->execute();
    $s_rooms = "";
    $s_msgs = "";
    $i_room = 0;
    $i_count = 0;
    $s_room = "";
    while (($a_msg = $r_res_rooms->fetchAssoc()) && $a_msg) {
      if ($a_msg['Room'] != $i_room) {
        if (!empty($s_room) && !empty($s_msgs)) {
          $s_rooms .= $this->parseXml($this->aXmlTemplates['history']['room'], $i_room, $s_room, $i_count) . $s_msgs . "</room>";
          $s_msgs = "";
          $i_count = 0;
        }
        $i_room = $a_msg['Room'];
        $s_room = $a_msg['Name'];
      }
      $i_count++;
      $s_msgs .= $this->parseXml($this->aXmlTemplates['history']['msg'], $a_msg['ID'], $a_msg['Sender'], $a_msg['Recipient'], $a_msg['Message']);
    }
    if (!empty($s_room) && !empty($s_msgs)) {
      $s_rooms .= $this->parseXml($this->aXmlTemplates['history']['room'], $i_room, $s_room, $i_count) . $s_msgs . "</room>";
    }
    $s_contents .= $this->makeGroup($s_rooms, "rooms");

    // Private dialogs.
    $s_private = "";
    $i_num_rows = (int) db_select($this->historyDbTable, 'h')->fields('h')->condition(db_and()->condition('Room', 0)->condition('Time', $i_start_date, '>=')->condition('Time', $i_end_date, '<'))->orderBy('SndRcp', 'ASC')->orderBy('Time', 'ASC')->countQuery()->execute()->fetchField();
    $r_res_msgs = db_select($this->historyDbTable, 'h')->fields('h')->condition(db_and()->condition('Room', 0)->condition('Time', $i_start_date, '>=')->condition('Time', $i_end_date, '<'))->orderBy('SndRcp', 'ASC')->orderBy('Time', 'ASC')->execute();
    if ($i_num_rows > 0) {
      $a_msg = $r_res_msgs->fetchAssoc();
      $s_private .= '<private sender="' . $a_msg['Sender'] . '" recipient="' . $a_msg['Recipient'] . '" count="';
      $s_msgs = parseXml($this->aXmlTemplates['history']['msg'], $a_msg['ID'], $a_msg['Sender'], $a_msg['Message']);
      $i_count = 1;
      $s_snd_rcp = $a_msg['SndRcp'];
      for ($i = 1; $i < $i_num_rows; $i++) {
        $a_msg = $r_res_msgs->fetchAssoc();
        if ($a_msg['SndRcp'] != $s_snd_rcp) {
          if (!empty($s_msgs)) {
            $s_private .= $i_count . '">' . $s_msgs . '</private><private sender="' . $a_msg['Sender'] . '" recipient="' . $a_msg['Recipient'] . '" count="';
            $s_msgs = "";
            $i_count = 0;
          }
          $s_snd_rcp = $a_msg['SndRcp'];
        }
        $i_count++;
        $s_msgs .= $this->parseXml($this->aXmlTemplates['history']['msg'], $a_msg['ID'], $a_msg['Sender'], $a_msg['Message']);
      }
      if (!empty($s_msgs)) {
        $s_private .= $i_count . '">' . $s_msgs . '</private>';
      }
    }
    $s_contents .= $this->makeGroup($s_private, "privates");
    return $s_contents;
  }

  /**
   * Retrieves history users.
   */
  private function getHistoryUsers($i_start_date, $i_end_date, $b_rooms_only) {
    $a_users = array();
    $o_cond = db_and()->condition('Time', $i_start_date, '>=')->condition('Time', $i_end_date, '<');
    if ($b_rooms_only) {
      $o_cond = $o_cond->condition('Room', 0, '<>');
    }
    $r_res = db_select($this->historyDbTable, 'h')->fields('h')->condition($o_cond)->orderBy('Room', 'ASC')->orderBy('Sender', 'ASC')->orderBy('Recipient', 'ASC')->execute();
    while (($a_msg = $r_res->fetchAssoc()) && $a_msg) {
      if (!empty($a_msg['Sender'])) {
        $a_users[] = $a_msg['Sender'];
      }
      if (!empty($a_msg['Recipient'])) {
        $a_users[] = $a_msg['Recipient'];
      }
    }
    $a_users = array_flip(array_unique($a_users));
    foreach ($a_users as $i_user_id => $s_value) {
      $a_users[$i_user_id] = $this->getUserInfo($i_user_id);
    }
    return $a_users;
  }

  /**
   * Uploads file.
   */
  public function actionUploadFile() {
    $s_sender = $this->getRequestVar("sender", "db");
    if (empty($s_sender)) {
      return;
    }
    if (is_uploaded_file($_FILES['Filedata']['tmp_name'])) {
      $s_file_path = $this->sFilesPath . $s_sender . ".temp";
      @unlink($s_file_path);
      move_uploaded_file($_FILES['Filedata']['tmp_name'], $s_file_path);
      @chmod($s_file_path, 0644);
    }
  }

  /**
   * Initiates file.
   */
  public function actionInitFile() {
    $s_sender = $this->getRequestVar("sender", "db");
    $s_file_path = $this->sFilesPath . $s_sender . ".temp";
    $s_contents = $this->parseXml($this->aXmlTemplates['result'], "msgErrorUpload", self::FAILED_VAL);
    if (empty($s_sender) || !file_exists($s_file_path) || filesize($s_file_path) == 0) {
      return $s_contents;
    }
    $i_count = $this->getRequestVar("count", "int");
    $s_rcp = $this->getRequestVar("recipient", "db");
    $s_message = $this->getRequestVar("message", "db");
    db_insert($this->messagesDbTable)->fields(array(
      'Count' => $i_count,
      'Sender' => $s_sender,
      'Recipient' => $s_rcp,
      'Message' => $s_message,
      'Type' => 'file',
      'Time' => time(),
    ))->execute();
    $s_file_name = Database::getConnection()->lastInsertId() . ".file";
    if (!@rename($s_file_path, $this->sFilesPath . $s_file_name)) {
      return $s_contents;
    }
    return $this->parseXml($this->aXmlTemplates['result'], $s_file_name, self::SUCCESS_VAL);
  }

  /**
   * Removes file.
   */
  public function actionRemoveFile() {
    $s_id = str_replace(".file", "", $this->sId);
    $this->removeFile($s_id);
  }

  /**
   * Gets help.
   */
  public function actionHelp() {
    $s_contents = $this->makeGroup("", "topics");
    $s_file_name = $this->sPath . "data/help.xml";
    if (file_exists($s_file_name)) {
      $r_handle = @fopen($s_file_name, "rt");
      $s_contents = @fread($r_handle, filesize($s_file_name));
      fclose($r_handle);
    }
    return $s_contents;
  }

  /**
   * Gets phrases.
   */
  public function actionGetPhrases() {
    $s_contents = $this->makeGroup("", "items");
    $s_file_name = $this->sPath . "data/phrases.xml";
    if (file_exists($s_file_name)) {
      $r_handle = @fopen($s_file_name, "rt");
      $s_contents = @fread($r_handle, filesize($s_file_name));
      fclose($r_handle);
    }
    return $s_contents;
  }

  /**
   * Uploads room background image.
   */
  public function actionUploadRoomImage() {
    @unlink($this->getRoomImage($this->sId));
    $a_size = getimagesize($_FILES['Filedata']['tmp_name']);
    if (is_uploaded_file($_FILES['Filedata']['tmp_name']) && $a_size) {
      switch ($a_size[2]) {
        case 1:
          $s_ext = "gif";
          break;

        case 2:
          $s_ext = "jpg";
          break;

        case 3:
          $s_ext = "png";
          break;

        default:
          $s_ext = "";
          break;
      }
      if (empty($s_ext)) {
        return;
      }
      $s_file_path = $this->sFilesPath . "room_" . $this->sId . "." . $s_ext;
      move_uploaded_file($_FILES['Filedata']['tmp_name'], $s_file_path);
      @chmod($s_file_path, 0644);
    }
  }

  /**
   * Checks room background image.
   */
  public function actionCheckRoomImage() {
    $s_file_path = $this->getRoomImage($this->sId);
    if (file_exists($s_file_path) && filesize($s_file_path) > 0) {
      return $this->parseXml($this->aXmlTemplates['result'], "", self::SUCCESS_VAL);
    }
    else {
      return $this->parseXml($this->aXmlTemplates['result'], "", self::FAILED_VAL);
    }
  }

  /**
   * Gets room background image.
   */
  public function actionRoomImage() {
    setlocale(LC_ALL, 'EN_US');
    header('Expires: ' . gmdate('D, d M Y H:i:s', time()) . ' GMT');
    $s_file_path = $this->getRoomImage($this->sId);
    if (filesize($s_file_path) > 0) {
      $s_ext = substr($s_file_path, -3);
      if ($s_ext == "jpg") {
        $s_ext = "jpeg";
      }
      header("Content-Type: image/" . $s_ext);
      readfile($s_file_path);
      die();
    }
  }

  /**
   * Gets room background image filename.
   */
  protected function getRoomImage($s_id) {
    $s_file = $this->sFilesPath . "room_" . $s_id . ".";
    if (file_exists($s_file . "jpg")) {
      return $s_file . "jpg";
    }
    if (file_exists($s_file . "png")) {
      return $s_file . "png";
    }
    if (file_exists($s_file . "gif")) {
      return $s_file . "gif";
    }
    return "";
  }

  /**
   * Gets memberships values.
   */
  protected function getMembershipValues($s_membership, $s_name = "") {
    $r_result = db_select($this->memsetsDbTable, 'ks')->fields('ks', array('ID', 'Def'));
    $r_result->leftJoin($this->memsDbTable, 'vals', 'ks.ID=vals.Setting');
    $r_result = $r_result->fields('vals', array('Value'))->condition('vals.Membership', $s_membership)->execute();
    $s_contents = '<membership id="' . $s_membership . '" ';
    while (($a_setting = $r_result->fetchAssoc()) && $a_setting) {
      $s_value = !isset($a_setting['Value']) || $a_setting['Value'] == "" ? $a_setting['Def'] : $a_setting['Value'];
      $s_contents .= 'v' . $a_setting['ID'] . '="' . $s_value . '" ';
    }
    $s_end = empty($s_name) ? '/>' : '><![CDATA[' . $s_name . ']]></membership>';
    $s_contents .= $s_end;
    return $s_contents;
  }

  /**
   * Gets memberships settings.
   */
  protected function getMembershipSettings($b_admin = FALSE) {
    $a_setting_tmpls = array(
      3 => '<setting id="#1#" name="#2#"><![CDATA[#3#]]></setting>',
      6 => '<setting id="#1#" name="#2#" type="#3#" default="#4#" range="#5#"><![CDATA[#6#]]></setting>',
    );
    $r_result = db_select($this->memsetsDbTable, 't')->fields('t')->execute();
    $s_settings = "";
    while (($a_setting = $r_result->fetchAssoc()) && $a_setting) {
      if ($b_admin) {
        $s_settings .= $this->parseXml($a_setting_tmpls, $a_setting['ID'], $a_setting['Name'], $a_setting['Type'], $a_setting['Def'], $a_setting['Scope'], $a_setting['Caption']);
      }
      else {
        $s_settings .= $this->parseXml($a_setting_tmpls, $a_setting['ID'], $a_setting['Name'], $a_setting['Error']);
      }
    }
    return $this->makeGroup($s_settings, "settings");
  }

  /**
   * Initializes user.
   */
  protected function initUser($a_user) {
    $a_profile = db_select($this->profilesDbTable, 't')->fields('t')->condition('ID', $a_user['id'])->execute()->fetchAssoc();
    if (!is_array($a_profile) || count($a_profile) == 0) {
      db_insert($this->profilesDbTable)->fields(array(
        'ID' => $a_user['id'],
        'Type' => $a_user['type'],
        'Smileset' => 'default',
      ))->execute();
    }
    else {
      $a_user['type'] = $a_profile["Type"];
    }
    $i_current_time = time();
    $i_count = (int) db_select($this->usersDbTable, 't')->fields('t')->condition('ID', $a_user['id'])->countQuery()->execute()->fetchField();
    if ($i_count) {
      db_update($this->usersDbTable)->fields(array(
        'Nick' => $a_user['nick'],
        'Sex' => $a_user['sex'],
        'Age' => $a_user['age'],
        'Description' => addslashes($a_user['desc']),
        'Photo' => $a_user['photo'],
        'Profile' => $a_user['profile'],
        'Start' => $i_current_time,
        'Time' => $i_current_time,
        'Status' => self::USER_STATUS_NEW,
      ))->condition('ID', $a_user['id'])->execute();
    }
    else {
      db_insert($this->usersDbTable, 't')->fields(array(
        'Nick' => $a_user['nick'],
        'Sex' => $a_user['sex'],
        'Age' => $a_user['age'],
        'Description' => addslashes($a_user['desc']),
        'Photo' => $a_user['photo'],
        'Profile' => $a_user['profile'],
        'Start' => $i_current_time,
        'Time' => $i_current_time,
        'Status' => self::USER_STATUS_NEW,
      ))->execute();
    }
    db_delete($this->roomsUsersDbTable)->condition('User', $a_user['id'])->execute();
    $r_files = db_select($this->messagesDbTable, 't')->fields('t')->condition(db_and()->condition('Recipient', $a_user['id'])->condition('Type', 'file'))->execute();
    while ($a_file = $r_files->fetchAssoc()) {
      $this->removeFile($a_file['ID']);
    }
    return $a_user;
  }

  /**
   * Ban user actions.
   */
  protected function doBan($s_switch, $s_id = "0") {
    switch ($s_switch) {
      case "check":
        return db_select($this->profilesDbTable, 't')->fields('t', array('Banned'))->condition('ID', $s_id)->execute()->fetchField() == self::TRUE_VAL;

      case "ban":
        $s_ban = self::TRUE_VAL;
        // Break shouldn't be here.
      case "unban":
        $s_ban = self::FALSE_VAL;

      default:
        $s_user_id = db_select($this->profilesDbTable, 't')->fields('t', array('ID'))->condition('ID', $s_id)->execute()->fetchField();
        if (empty($s_user_id)) {
          return db_insert($this->profilesDbTable)->fields(array(
            'ID' => $s_id,
            'Banned' => $s_ban,
            'Type' => self::CHAT_TYPE_FULL,
          ))->execute();
        }
        else {
          return db_update($this->profilesDbTable)->fields(array('Banned' => $s_ban))->condition('ID', $this->sId)->execute() > 0;
        }
    }
  }

  /**
   * Gets rooms helper.
   */
  protected function getRooms($s_mode = 'new', $s_id = "", $i_last_update = 0) {
    $s_rooms = "";
    switch ($s_mode) {
      case "update":
		$r_result = db_query("SELECT * FROM {rzchat_rooms} WHERE IF(':id'='0', 1, OwnerID<>'" . $s_id . "') AND (Time >= " . $i_last_update . ") ORDER BY Time", array(':id' => $s_id));
        while ($a_room = $r_result->fetchAssoc()) {
          switch ($a_room['Status']) {
            case self::ROOM_STATUS_DELETE:
              $s_rooms .= $this->parseXml($this->aXmlTemplates['room'], $a_room['ID'], self::ROOM_STATUS_DELETE);
              break;

            case self::ROOM_STATUS_NORMAL:
            default:
              $s_rooms .= $this->parseXml($this->aXmlTemplates['room'], $a_room['ID'], self::ROOM_STATUS_NORMAL, $a_room['OwnerID'], empty($a_room['Password']) ? self::FALSE_VAL : self::TRUE_VAL, stripslashes($a_room['Name']), stripslashes($a_room['Description']));
              break;
          }
        }
        break;

      case "updateUsers":
        $s_sql = "SELECT `r`.`ID` AS `RoomID`, GROUP_CONCAT(DISTINCT IF(`ru`.`Status`='" . self::ROOM_STATUS_NORMAL . "',`ru`.`User`,'') SEPARATOR ',') AS `In`, GROUP_CONCAT(DISTINCT IF(`ru`.`Status`='" . self::ROOM_STATUS_DELETE . "',`ru`.`User`,'') SEPARATOR ',') AS `Out` FROM `" . $this->roomsDbTable . "` AS `r` INNER JOIN `" . $this->roomsUsersDbTable . "` AS `ru` WHERE `r`.`ID`=`ru`.`Room` AND `r`.`Status`='" . self::ROOM_STATUS_NORMAL . "' AND `ru`.`Time`>=" . $i_last_update . " GROUP BY `r`.`ID`";
        $r_result = db_query($s_sql);
        while ($a_room = $r_result->fetchAssoc()) {
          $s_rooms .= $this->parseXml($this->aXmlTemplates['room'], $a_room['RoomID'], $a_room['UsersIn'], $a_room['UsersOut']);
        }
        break;

      case "all":
        $i_current_time = time() - floor($this->getRequestVar("_t", "int") / 1000);
        $i_count = db_select($this->roomsUsersDbTable, 'u')->fields('u')->countQuery()->execute()->fetchField();
        if ($i_count == 0) {
          db_query("TRUNCATE TABLE {rzchat_rooms_users}");
        }
        $i_rooms_count = db_select($this->roomsDbTable, 'r')->fields('r')->condition('Status', self::ROOM_STATUS_NORMAL)->countQuery()->execute()->fetchField();
        if (empty($i_rooms_count)) {
          db_insert($this->roomsDbTable)->fields(array(
            'Name' => 'Lobby',
            'OwnerID' => 0,
            'Description' => 'Welcome to our chat!',
            'Time' => 0,
            'Status' => self::ROOM_STATUS_NORMAL,
          ))->execute();
        }
        $s_sql = "SELECT `r`.`ID` AS `RoomID`, `r`.*, GROUP_CONCAT(DISTINCT IF(`ru`.`Status`='" . self::ROOM_STATUS_NORMAL . "' AND `ru`.`User`<>'" . $s_id . "',`ru`.`User`,'') SEPARATOR ',') AS `In`, GROUP_CONCAT(DISTINCT IF(`ru`.`Status`='" . self::ROOM_STATUS_NORMAL . "' AND `ru`.`User`<>'" . $s_id . "',(" . $i_current_time . "-`ru`.`Time`),'') SEPARATOR ',') AS `InTime` FROM `" . $this->roomsDbTable . "` AS `r` LEFT JOIN `" . $this->roomsUsersDbTable . "` AS `ru` ON `r`.`ID`=`ru`.`Room` WHERE `r`.`Status`='" . self::ROOM_STATUS_NORMAL . "' GROUP BY `r`.`ID` ORDER BY `r`.`ID` LIMIT " . (int) $this->getSettingValue("maxRoomsNumber");
        $r_result = db_query($s_sql);
        while (($a_room = $r_result->fetchAssoc()) && $a_room) {
          $s_rooms .= $this->parseXml($this->aXmlTemplates['room'], $a_room['RoomID'], $a_room['OwnerID'], empty($a_room['Password']) ? self::FALSE_VAL : self::TRUE_VAL, stripslashes($a_room['Name']), stripslashes($a_room['Description']), $a_room['In'], $a_room['InTime']);
        }
        break;
    }
    return $s_rooms;
  }

  /**
   * Rooms actions helper.
   */
  protected function doRoom($s_switch, $s_user_id = "", $i_room_id = 0, $s_title = "", $s_password = "", $s_desc = "", $b_temp = FALSE) {
    $i_current_time = time();
    switch ($s_switch) {
      case "insert":
        $a_cur_room = db_select($this->roomsDbTable, 'r')->fields('r')->condition('Name', $s_title)->execute()->fetchAssoc();
        $s_status = $b_temp ? self::ROOM_STATUS_DELETE : self::ROOM_STATUS_NORMAL;
        if (!empty($a_cur_room['ID']) && $s_user_id == $a_cur_room['OwnerID']) {
          db_update($this->roomsDbTable)->fields(array(
            'Name' => $s_title,
            'Password' => $s_password,
            'Description' => $s_desc,
            'OwnerID' => $s_user_id,
            'Time' => $i_current_time,
            'Status' => $s_status,
          ))->condition('ID', $a_cur_room['ID'])->execute();
          return $a_cur_room['ID'];
        }
        elseif (empty($a_cur_room['ID'])) {
          db_insert($this->roomsDbTable)->fields(array(
            'ID' => $i_room_id,
            'Name' => $s_title,
            'Password' => $s_password,
            'Description' => $s_desc,
            'OwnerID' => $s_user_id,
            'Time' => $i_current_time,
            'Status' => $s_status,
          ))->execute();
          return Database::getConnection()->lastInsertId();
        }
        else {
          return 0;
        }
        break;

      case "update":
        db_update($this->roomsDbTable)->fields(array(
          'Name' => $s_title,
          'Password' => $s_password,
          'Description' => $s_desc,
          'Time' => $i_current_time,
          'Status' => self::ROOM_STATUS_NORMAL,
        ))->condition('ID', $i_room_id)->execute();
        break;

      case "delete":
        db_update($this->roomsDbTable)->fields(array('Time' => $i_current_time, 'Status' => self::ROOM_STATUS_DELETE))->condition('ID', $i_room_id)->execute();
        break;

      case "enter":
        $s_id = db_select($this->roomsUsersDbTable, 't')->fields('t', array('ID'))->condition(db_and()->condition('Room', $i_room_id)->condition('User', $s_user_id))->execute()->fetchField();
        if (empty($s_id)) {
          db_insert($this->roomsUsersDbTable)->fields(array(
            'Room' => $i_room_id,
            'User' => $s_user_id,
            'Time' => $i_current_time,
          ))->execute();
        }
        else {
          db_update($this->roomsUsersDbTable)->fields(array('Time' => $i_current_time, 'Status' => self::ROOM_STATUS_NORMAL))->condition('ID', $s_id)->execute();
        }
        break;

      case "exit":
        db_update($this->roomsUsersDbTable)->fields(array('Time' => $i_current_time, 'Status' => self::ROOM_STATUS_DELETE))->condition(db_and()->condition('Room', $i_room_id)->condition('User', $s_user_id))->range(0, 1)->execute();
        break;

      case "deleteTemp":
        db_delete($this->roomsDbTable)->condition(db_and()->condition('Status', self::ROOM_STATUS_DELETE)->condition('Time', $i_current_time - 86400))->execute();
        break;
    }
  }

  /**
   * Gets last update of the user.
   */
  protected function getLastUpdate($s_id) {
    return (int) db_select($this->usersDbTable, 't')->fields('t', array('Time'))->condition('ID', $s_id)->execute()->fetchField();
  }

  /**
   * Refreshes users info.
   */
  protected function refreshUsersInfo($s_id = "", $s_mode = 'all', $i_last_update = 0) {
    $i_update_interval = (int) $this->getSettingValue("updateInterval");
    $i_idle_time = 300;
    $i_delete_time = 500;
    $s_content = "";
    $i_current_time = time();
    if (empty($i_last_update)) {
      $i_last_update = $i_current_time;
    }
    $i_users_not_updated = (int) db_select($this->usersDbTable, 't')->fields('t')->condition(db_and()->condition('Status', array(self::USER_STATUS_KICK, self::USER_STATUS_IDLE), 'NOT IN')->condition('Time', $i_last_update, '<'))->countQuery()->execute()->fetchField();
    $a_update = array('Time' => $i_current_time);
    if ($i_users_not_updated > 0) {
      $a_update['Status'] = self::USER_STATUS_OLD;
    }
    db_update($this->usersDbTable)->fields($a_update)->condition(db_and()->condition('ID', $s_id)->condition('Status', self::USER_STATUS_KICK, '<>')->condition(db_or()->condition('Status', array(
      self::USER_STATUS_NEW,
      self::USER_STATUS_TYPE,
      self::USER_STATUS_ONLINE,
    ), 'NOT IN')->condition('Time', $i_current_time - $i_update_interval, '<')))->execute();
    db_update($this->usersDbTable)->fields(array('Time' => $i_current_time, 'Status' => self::USER_STATUS_IDLE))->condition(db_and()->condition('Status', self::USER_STATUS_IDLE)->condition('Time', $i_current_time - $i_idle_time, '<='))->execute();

    db_delete($this->roomsUsersDbTable)->condition(db_and()->condition('Status', self::ROOM_STATUS_DELETE)->condition('Time', $i_current_time - $i_delete_time))->execute();
    $r_files = db_select($this->messagesDbTable, 'files');
    $r_files->join($this->usersDbTable, 'users', 'files.Recipient=users.ID');
    $r_files = $r_files->fields('files', array('ID'))->condition(db_and()->condition('files.Type', 'file')->condition('users.Status', self::USER_STATUS_IDLE)->condition('users.Time', $i_current_time - $i_delete_time, '<='))->execute();
    while ($a_file = $r_files->fetchAssoc()) {
      $this->removeFile($a_file['ID']);
    }
    db_delete($this->usersDbTable)->condition(db_and()->condition('Status', self::USER_STATUS_IDLE)->condition('Time', $i_current_time - $i_delete_time, '<='))->execute();
    // Delete old rooms.
    db_delete($this->roomsDbTable)->condition(db_and()->condition('Status', self::ROOM_STATUS_DELETE)->condition('Time', $i_current_time - $i_delete_time, '<='))->execute();
    // Delete old messages.
    db_delete($this->messagesDbTable)->condition(db_and()->condition('Type', 'text')->condition('Time', $i_current_time - $i_delete_time))->execute();
    // Get information about users in the chat.
    switch ($s_mode) {
      case "update":
        $r_res = db_select($this->profilesDbTable, 'rp')->join($this->usersDbTable, 'ccu', 'rp.ID=ccu.ID')->fields('files', array('ID'))->condition('files.Type', 'file')->orderBy('ccu.Time', 'ASC')->execute();
        while ($a_user = $r_res->fetchAssoc()) {
          if ($a_user['ID'] == $s_id && !($a_user['Status'] == self::USER_STATUS_KICK || $a_user['Status'] == self::USER_STATUS_TYPE)) {
            continue;
          }
        }
        switch ($a_user['Status']) {
          case self::USER_STATUS_NEW:
            $s_content .= $this->parseXml($this->aXmlTemplates['user'], $a_user['ID'], $a_user['Status'], $a_user['Nick'], $a_user['Sex'], $a_user['Age'], stripslashes($a_user['Description']), $a_user['Photo'], $a_user['Profile'], $a_user['Type'], $a_user['Online']);
            break;

          case self::USER_STATUS_TYPE:
            $s_content .= $this->parseXml($this->aXmlTemplates['user'], $a_user['ID'], $a_user['Status'], $a_user['Type']);
            break;

          case self::USER_STATUS_ONLINE:
            $s_content .= $this->parseXml($this->aXmlTemplates['user'], $a_user['ID'], $a_user['Status'], $a_user['Type'], $a_user['Online']);
            break;

          case self::USER_STATUS_IDLE:
          case self::USER_STATUS_KICK:
            $s_content .= $this->parseXml($this->aXmlTemplates['user'], $a_user['ID'], $a_user['Status']);
            break;
        }
        break;

      case 'all':
        $i_current_time -= floor($this->getRequestVar("_t", "int") / 1000);
        $r_res = db_select($this->profilesDbTable, 'rp');
        $r_res->join($this->usersDbTable, 'ccu', 'rp.ID=ccu.ID');
        $r_res = $r_res->fields('rp')->fields('ccu')->condition(db_and()->condition('ccu.Status', array(self::USER_STATUS_IDLE, self::USER_STATUS_KICK), 'NOT IN')->condition('rp.Banned', self::FALSE_VAL))->orderBy('ccu.Start', 'DESC')->execute();
        while ($a_user = $r_res->fetchAssoc()) {
          $s_content .= $this->parseXml($this->aXmlTemplates['user'], $a_user['ID'], self::USER_STATUS_NEW, $a_user['Nick'], $a_user['Sex'], $a_user['Age'], stripslashes($a_user['Description']), $a_user['Photo'], $a_user['Profile'], $a_user['Type'], $a_user['Online'], ($i_current_time - $a_user['Time']));
        }
        break;
    }
    return $this->makeGroup($s_content, "users");
  }

  /**
   * Removing file helper.
   */
  protected function removeFile($s_file_id) {
    db_delete($this->messagesDbTable)->condition('ID', $s_file_id)->execute();
    @unlink($this->sPath . "files/" . $s_file_id . ".file");
  }

}
