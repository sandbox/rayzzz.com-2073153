<?php

/**
 * @file
 * Module integration class.
 */

class RzchatIntegration extends RzchatBase {
  /**
   * Site home url.
   */
  protected $sSiteUrl;
  /**
   * Site login user url.
   */
  protected $sLoginUrl;

  /**
   * Class constructor.
   */
  public function __construct($s_path, $s_url) {
    parent::__construct($s_path, $s_url);
    $a_url = explode("sites/", $this->sUrl);
    $this->sLoginUrl = $this->sSiteUrl = $a_url[0];
  }

  /**
   * Checks user login and password.
   */
  protected function loginUser($s_name, $s_password, $b_login = FALSE) {
    $s_field = $b_login ? "name" : "uid";
    $s_id = db_select('users', 't')->fields('t', array('uid'))->condition(db_and()->condition($s_field, $s_name)->condition('pass', $s_password))->execute()->fetchField();
    return !empty($s_id);
  }

  /**
   * Checks admin login and password.
   */
  protected function loginAdmin($s_id, $s_password) {
    if ($this->loginUser($s_id, $s_password)) {
      return user_access('administer');
    }
    return FALSE;
  }

  /**
   * Gets user info.
   */
  protected function getUserInfo($s_id, $b_nick = FALSE) {
    $a_user = db_select('users', 't')->fields('t')->condition($b_nick ? 'name' : 'uid', $s_id)->execute()->fetchAssoc();
    $s_sex = "M";
    $s_nick = $s_desc = $a_user['name'];
    $s_age = "25";
    $s_profile = $this->sSiteUrl . "?q=user/" . $a_user["uid"];
    $i_pic_id = (int) $a_user['picture'];
    if ($i_pic_id > 0) {
      $s_photo = db_select('file_managed', 't')->fields('t', array('filename'))->condition('fid', $i_pic_id)->execute()->fetchField();
    }
    $s_photo = empty($s_photo) ? $this->sUrl . "data/" . ($s_sex == "M" ? "male.jpg" : "female.jpg") : $this->sSiteUrl . "sites/default/files/styles/thumbnail/public/pictures/" . $s_photo;
    return array(
      "id" => (int) $a_user["uid"],
      "nick" => $s_nick,
      "sex" => $s_sex,
      "age" => $s_age,
      "desc" => $s_desc,
      "photo" => $s_photo,
      "profile" => $s_profile,
    );
  }

  /**
   * Searches for user.
   */
  protected function searchUser($s_value, $s_field = "ID") {
    if ($s_field == "ID") {
      $s_field = "uid";
    }
    else {
      $s_field = "name";
    }
    $s_id = db_select('users', 't')->fields('t', array('uid'))->condition($s_field, $s_value)->execute()->fetchField();
    return $s_id;
  }

  /**
   * Gets current language.
   */
  protected function getCurrentLang($s_user_id = "") {
    $s_lang = db_select('users', 't')->fields('t', array('language'))->condition('uid', $s_user_id)->execute()->fetchField();
    if (!empty($s_lang)) {
      $b_enabled = (int) db_select('languages', 't')->fields('t', array('enabled'))->condition('language', $s_lang)->execute()->fetchField();
      if ($b_enabled) {
        return $s_lang;
      }
    }
    $s_lang = unserialize(db_select('variable', 't')->fields('t', array('value'))->condition('name', 'language_default')->execute()->fetchField());
    if (!empty($s_lang)) {
      return $s_lang;
    }
    return parent::getCurrentLang($s_user_id);
  }

  /**
   * Gets membership id by user id.
   */
  protected function getMembershipId($s_user_id) {
    $i_user_id = (int) $s_user_id;
    if (empty($i_user_id)) {
      return 1;
    }
    $i_mem = (int) db_select('users_roles', 't')->fields('t', array('rid'))->condition('uid', $i_user_id)->execute()->fetchField();
    if (empty($i_mem)) {
      $i_mem = 2;
    }
    return $i_mem;
  }

  /**
   * Gets memberships levels.
   */
  protected function getMemberships() {
    $a_memberships = array();
    $r_result = db_select('role', 't')->fields('t')->execute();
    while ($a_membership = $r_result->fetchAssoc()) {
      $a_memberships[$a_membership["rid"]] = $a_membership["name"];
    }
    return $a_memberships;
  }

}
